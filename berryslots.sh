#!/bin/bash

brr=allberries.bash

echo 'Welcome to BerrySlots! Type !slots to play.'

while read i
do
	if [ "$i" = "!slots" ]
	then
		fortune $brr > /tmp/Abrr.$$
		fortune $brr > /tmp/Bbrr.$$
		fortune $brr > /tmp/Cbrr.$$

		A=`sed 1q /tmp/Abrr.$$`
		B=`sed 1q /tmp/Bbrr.$$`
		C=`sed 1q /tmp/Cbrr.$$`

		paste -d\  /tmp/{A,B,C}brr.$$

		[[ "x$A" = "x$B" ]] && [[ "x$A" = "x$C" ]] && echo You win!! || echo Try again!

		rm -f /tmp/{A,B,C}brr.$$
	fi
done
